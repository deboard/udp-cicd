# To contribute improvements to CI/CD templates, please follow the Development guide at:
# https://docs.gitlab.com/ee/development/cicd/templates.html
# This specific template is located at:
# https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Python.gitlab-ci.yml
# Official language image. Look for the different tagged releases at:
# https://hub.docker.com/r/library/python/tags/

image: python:latest

# Change pip's cache directory to be inside the project directory since we can
# only cache local items.
variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

# https://pip.pypa.io/en/stable/topics/caching/
cache:
  paths:
    - .cache/pip

before_script:
  - python --version ; pip --version  # For debugging
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate

stages:
  - formatting
  - lint
  - compile
  - test
  - semgrep

format:
  stage: formatting
  image: registry.gitlab.com/mafda/python-linting
  script:
    - find . -maxdepth 2 -type f -name "*.py" | xargs black --check  

pylint:
  stage: lint
  image: registry.gitlab.com/mafda/python-linting
  script:
    - find . -maxdepth 2 -type f -name "*.py" | xargs pylint

compile:
  stage: compile
  script:
    - find . -maxdepth 2 -type f -name "*.py" | xargs python3 -m py_compile 

test:
  stage: test
  script:
    - python3 unit_test.py

## see: https://www.youtube.com/watch?v=ObpgD-PxSWY
semgrep:
  stage: semgrep
  # A Docker image with Semgrep installed.
  image: returntocorp/semgrep
  # Run the "semgrep ci" command on the command line of the docker image.
  script: semgrep ci

  rules:
  # Scan changed files in MRs, (diff-aware scanning):
  - if: $CI_MERGE_REQUEST_IID

  # Scan mainline (default) branches and report all findings.
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

  variables:
    # Connect to Semgrep Cloud Platform through your SEMGREP_APP_TOKEN.
    # Generate a token from Semgrep Cloud Platform > Settings
    # and add it as a variable in your GitLab CI/CD project settings.
    SEMGREP_APP_TOKEN: $SEMGREP_APP_TOKEN
