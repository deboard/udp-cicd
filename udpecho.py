#!/usr/bin/env python3

"""'
  Description: template
"""

__author__    = 'John DeBoard'
__email__     = 'john.deboard.ctr@us.af.mil'
__date__      = '2024-01-25'
__version__   = '1.0.0'

import socket

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--file',
                    type=str,
                    help='Name of input file.',
                    required=False)
    args = parser.parse_args()

    # Author: David Manouchehri <manouchehri@protonmail.com>

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

    server_address = '0.0.0.0'
    server_port = 33333

    server = (server_address, server_port)
    sock.bind(server)
    print("Listening on " + server_address + ":" + str(server_port))

    while True:
        payload, client_address = sock.recvfrom(1)
        print("Echoing back to " + str(client_address))
        sent = sock.sendto(payload, client_address)

